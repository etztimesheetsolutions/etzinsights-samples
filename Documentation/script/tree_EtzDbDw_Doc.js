USETEXTLINKS = 1
STARTALLOPEN = 0
HIGHLIGHT = 1
HIGHLIGHT_BG = "gray"
PRESERVESTATE = 1
GLOBALTARGET="R"
BUILDALL = 0
USEICONS = 0
USEFRAMES = 1
WRAPTEXT = 0
ICONPATH = "images/"

foldersTree = gFld("Database Documentation", "SqlServer.ETZDEV_TIML01.EtzDbDw_Doc/default.htm")
f0 = gFld("ETZDEV_TIML01.EtzDbDw_Doc (SQL Server)", "SqlServer.ETZDEV_TIML01.EtzDbDw_Doc/default.htm")
f0_0 = gFld("Tables", "SqlServer.ETZDEV_TIML01.EtzDbDw_Doc/allTables.htm")
f0_0_0 = gFld("dbo", "")
f0_0_0_0 = gFld("Agency", "SqlServer.ETZDEV_TIML01.EtzDbDw_Doc/table_dboAgency.htm")
f0_0_0_1 = gFld("Assignment", "SqlServer.ETZDEV_TIML01.EtzDbDw_Doc/table_dboAssignment.htm")
f0_0_0_2 = gFld("AssignmentAuthorisation", "SqlServer.ETZDEV_TIML01.EtzDbDw_Doc/table_dboAssignmentAuthorisation.htm")
f0_0_0_3 = gFld("AssignmentCommission", "SqlServer.ETZDEV_TIML01.EtzDbDw_Doc/table_dboAssignmentCommission.htm")
f0_0_0_4 = gFld("AssignmentConsultant", "SqlServer.ETZDEV_TIML01.EtzDbDw_Doc/table_dboAssignmentConsultant.htm")
f0_0_0_5 = gFld("AssignmentConsultantSplit", "SqlServer.ETZDEV_TIML01.EtzDbDw_Doc/table_dboAssignmentConsultantSplit.htm")
f0_0_0_6 = gFld("AssignmentRate", "SqlServer.ETZDEV_TIML01.EtzDbDw_Doc/table_dboAssignmentRate.htm")
f0_0_0_7 = gFld("BillInvoice", "SqlServer.ETZDEV_TIML01.EtzDbDw_Doc/table_dboBillInvoice.htm")
f0_0_0_8 = gFld("Branch", "SqlServer.ETZDEV_TIML01.EtzDbDw_Doc/table_dboBranch.htm")
f0_0_0_9 = gFld("Candidate", "SqlServer.ETZDEV_TIML01.EtzDbDw_Doc/table_dboCandidate.htm")
f0_0_0_10 = gFld("Client", "SqlServer.ETZDEV_TIML01.EtzDbDw_Doc/table_dboClient.htm")
f0_0_0_11 = gFld("Document", "SqlServer.ETZDEV_TIML01.EtzDbDw_Doc/table_dboDocument.htm")
f0_0_0_12 = gFld("ExchangeRate", "SqlServer.ETZDEV_TIML01.EtzDbDw_Doc/table_dboExchangeRate.htm")
f0_0_0_13 = gFld("PayInvoice", "SqlServer.ETZDEV_TIML01.EtzDbDw_Doc/table_dboPayInvoice.htm")
f0_0_0_14 = gFld("PurchaseOrder", "SqlServer.ETZDEV_TIML01.EtzDbDw_Doc/table_dboPurchaseOrder.htm")
f0_0_0_15 = gFld("Question", "SqlServer.ETZDEV_TIML01.EtzDbDw_Doc/table_dboQuestion.htm")
f0_0_0_16 = gFld("QuestionAnswer", "SqlServer.ETZDEV_TIML01.EtzDbDw_Doc/table_dboQuestionAnswer.htm")
f0_0_0_17 = gFld("Sector", "SqlServer.ETZDEV_TIML01.EtzDbDw_Doc/table_dboSector.htm")
f0_0_0_18 = gFld("Supplier", "SqlServer.ETZDEV_TIML01.EtzDbDw_Doc/table_dboSupplier.htm")
f0_0_0_19 = gFld("SupplierUser", "SqlServer.ETZDEV_TIML01.EtzDbDw_Doc/table_dboSupplierUser.htm")
f0_0_0_20 = gFld("TimesheetDay", "SqlServer.ETZDEV_TIML01.EtzDbDw_Doc/table_dboTimesheetDay.htm")
f0_0_0_21 = gFld("TransactionItem", "SqlServer.ETZDEV_TIML01.EtzDbDw_Doc/table_dboTransactionItem.htm")
f0_0_0_22 = gFld("TransactionItemClientAuthoriser", "SqlServer.ETZDEV_TIML01.EtzDbDw_Doc/table_dboTransactionItemClientAuthoriser.htm")
f0_0.addChildren([f0_0_0])
f0_0_0.addChildren([f0_0_0_0,f0_0_0_1,f0_0_0_2,f0_0_0_3,f0_0_0_4,f0_0_0_5,f0_0_0_6,f0_0_0_7,f0_0_0_8,f0_0_0_9,f0_0_0_10,f0_0_0_11,f0_0_0_12,f0_0_0_13,f0_0_0_14,f0_0_0_15,f0_0_0_16,f0_0_0_17,f0_0_0_18,f0_0_0_19,f0_0_0_20,f0_0_0_21,f0_0_0_22])
f0.addChildren([f0_0])

foldersTree.addChildren([f0])
foldersTree.treeID = "L1"

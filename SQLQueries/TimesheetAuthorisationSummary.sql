SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED; SET ANSI_WARNINGS OFF

DECLARE @StartDate DATETIME = '2015-01-01';
DECLARE @EndDate DATETIME = '2015-02-01';
DECLARE @SortBy INT = 0;

DECLARE @TotalAuthorised TABLE (Total FLOAT)
INSERT INTO @TotalAuthorised
SELECT
 COUNT(DISTINCT TransactionItem.TransactionId) as [Total]
FROM
 TransactionItem
WHERE 
 TransactionItem.StatusId = 2
 AND ((@SortBy = 0 AND (TransactionItem.EndDate BETWEEN @StartDate AND @EndDate))
    OR (@SortBy = 1 AND TransactionItem.StatusChangeDate BETWEEN @StartDate AND DATEADD(d, 1, @EndDate)))

DECLARE @TotalByType TABLE (AuthType NVARCHAR(100), AuthTotal FLOAT)
INSERT INTO @TotalByType
SELECT 
 TransactionItem.AuthorisationTypeName,
 COUNT(DISTINCT TransactionItem.TransactionId) as [Count] 
FROM 
 TransactionItem
WHERE 
 TransactionItem.StatusId = 2
 AND ((@SortBy = 0 AND (TransactionItem.EndDate BETWEEN @StartDate AND @EndDate))
 OR (@SortBy = 1 AND TransactionItem.StatusChangeDate BETWEEN @StartDate AND DATEADD(d, 1, @EndDate)))
 AND TransactionItem.AgencyId = 295
GROUP BY 
 TransactionItem.AuthorisationTypeName
ORDER BY 
 TransactionItem.AuthorisationTypeName

DECLARE @BreakdownTotals TABLE (ClientID INT,
        AuthorisationType NVARCHAR(250), UserTotalByAuthID FLOAT) 
INSERT INTO @BreakdownTotals
SELECT 
 Client.ClientId,
 TransactionItem.AuthorisationTypeName,
 COUNT(DISTINCT TransactionItem.TransactionId) as [Count] 
FROM 
 TransactionItem
 INNER JOIN Assignment ON TransactionItem.AssignmentId = Assignment.AssignmentId
 INNER JOIN Client ON Assignment.ClientId = Client.ClientID
WHERE 
 TransactionItem.StatusId = 2
 AND ((@SortBy = 0 AND (TransactionItem.EndDate BETWEEN @StartDate AND @EndDate))
 OR (@SortBy = 1 AND TransactionItem.StatusChangeDate BETWEEN @StartDate AND DATEADD(d, 1, @EndDate)))
GROUP BY 
 Client.ClientId, TransactionItem.AuthorisationTypeName, Client.Name
ORDER BY
 TransactionItem.AuthorisationTypeName, Client.Name;

SELECT
 TotalsByType.AuthType AS [Authorisation Type],
 Client.Name AS Client,
 MAX(TotalsByType.AuthTotal) AS [Total for Type],
 MAX(TotalsByType.AuthTotal/TotalAuthorised.Total) AS [Total for Type (%)],
 SUM(BreakdownTotals.UserTotalByAuthID) AS [Client Total for Type],
 SUM(BreakdownTotals.UserTotalByAuthId/TotalAuthorised.Total) AS [Client total for Type (%)]
FROM @TotalByType AS TotalsByType
 INNER JOIN @BreakdownTotals AS BreakdownTotals ON TotalsByType.AuthType = BreakdownTotals.AuthorisationType
 INNER JOIN Client ON BreakdownTotals.ClientID = Client.ClientId
 CROSS JOIN @TotalAuthorised AS TotalAuthorised
GROUP BY TotalsByType.AuthType, Client.Name
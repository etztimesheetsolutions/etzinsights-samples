SET NOCOUNT ON;
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
SET ANSI_WARNINGS OFF;

DECLARE @StartDate DATETIME = '2015-01-01';
DECLARE @EndDate DATETIME = '2015-02-01';

DECLARE @TransactionItemDistinct TABLE (AgencyId INT, TransactionId INT, StatusId INT, [Status] NVARCHAR(MAX), EndDate DATETIME, StatusChangeDate DATETIME,
        BillRateCodeDescription NVARCHAR(MAX), BillCode NVARCHAR(MAX), BillBaseExchangeRateFromCurrency NVARCHAR(MAX),
        BillBillingUnit INT, BillUnits DECIMAL(18, 2), BillDescription NVARCHAR(MAX), BillRate DECIMAL(18, 2), 
        BillTotalNetSalesTax DECIMAL(18, 2), BillSalesTaxAmt DECIMAL(18, 2), BillSalesTaxRate DECIMAL(18, 2),
        BillTotalGross DECIMAL(18, 2), PayRateCodeDescription NVARCHAR(MAX), PayCode NVARCHAR(MAX), PayBaseExchangeRateFromCurrency NVARCHAR(MAX),
        PayCostOfSaleAmt DECIMAL(18, 2), PayBillingUnit INT, PayUnits DECIMAL(18, 2), PayDescription NVARCHAR(MAX), PayRate DECIMAL(18, 2),
        PayTotalNetOfSalesTax DECIMAL(18, 2), PaySalesTaxAmt DECIMAL(18, 2), PaySalesTaxRate DECIMAL(18, 2), PayTotalGross DECIMAL(18, 2),
        BillInvoiceId INT, PayInvoiceId INT, PayRunId INT, AssignmentId INT, BillBaseExchangeRateId INT, PayBaseExchangeRateId INT,
        PurchaseOrderId INT, PayCommissionInvoiceId INT)
INSERT INTO @TransactionItemDistinct
SELECT DISTINCT
 TransactionItem.AgencyId,
 TransactionItem.TransactionId,
 TransactionItem.StatusId,
 TransactionItem.StatusName,
 TransactionItem.EndDate,
 TransactionItem.StatusChangeDate,
 TransactionItem.BillRateCodeDescription,
 TransactionItem.BillCode,
 TransactionItem.BillBaseExchangeRateFromCurrency,
 TransactionItem.BillBillingUnit,
 TransactionItem.BillUnits,
 TransactionItem.BillDescription,
 TransactionItem.BillRate,
 TransactionItem.BillTotalNetSalesTax,
 TransactionItem.BillSalesTaxAmt,
 TransactionItem.BillSalesTaxRate,
 TransactionItem.BillTotalGross,
 TransactionItem.PayRateCodeDescription,
 TransactionItem.PayCode,
 TransactionItem.PayBaseExchangeRateFromCurrency,
 TransactionItem.PayCostOfSaleAmt,
 TransactionItem.PayBillingUnit,
 TransactionItem.PayUnits,
 TransactionItem.PayDescription,
 TransactionItem.PayRate,
 TransactionItem.PayTotalNetOfSalesTax,
 TransactionItem.PaySalesTaxAmt,
 TransactionItem.PaySalesTaxRate,
 TransactionItem.PayTotalGross,
 TransactionItem.BillInvoiceId,
 TransactionItem.PayInvoiceId,
 TransactionItem.PayRunId,
 TransactionItem.AssignmentId,
 TransactionItem.BillBaseExchangeRateId,
 TransactionItem.PayBaseExchangeRateId,
 TransactionItem.PurchaseOrderId,
 TransactionItem.PayCommissionInvoiceId
FROM TransactionItem
WHERE
 TransactionItem.StatusChangeDate BETWEEN @StartDate AND @EndDate AND
 ((TransactionItem.BillInvoiceId = 0 AND TransactionItem.BillTotalGross != 0)
 OR (TransactionItem.PayInvoiceId = 0 AND TransactionItem.PayRunId = 0 AND TransactionItem.PayTotalGross != 0))
 AND TransactionItem.StatusId = 2
 AND TransactionItem.AgencyId = 295

SELECT
 Candidate.AgencyRefExport AS CandidateRef,
 Candidate.FirstName + ' ' + Candidate.LastName AS Candidate,
 Candidate.FirstName AS CandidateFirstName,
 Candidate.LastName AS CandidateLastName,
 Assignment.BillingNotes,
 Consultant.FirstName + ' ' + Consultant.LastName AS Consultant,
 ISNULL(Sector.Description, '') AS Sector,
 ISNULL(Branch.Description, '') AS Branch,
 ISNULL(Assignment.RefOptionType1Text,'') AS AssignmentCategory1Question,
 ISNULL(Assignment.RefOptionType1Answer, '') AS AssignmentCategory1Answer,
 ISNULL(Assignment.RefOptionType2Text,'') AS AssignmentCategory2Question,
 ISNULL(Assignment.RefOptionType2Answer, '') AS AssignmentCategory2Answer,
 ISNULL(Assignment.RefOptionType3Text,'') AS AssignmentCategory3Question,
 ISNULL(Assignment.RefOptionType3Answer, '') AS AssignmentCategory3Answer,
 ISNULL(Assignment.RefOptionType4Text,'') AS AssignmentCategory4Question,
 ISNULL(Assignment.RefOptionType4Answer, '') AS AssignmentCategory4Answer,
 ISNULL(Assignment.RefOptionType5Text,'') AS AssignmentCategory5Question,
 ISNULL(Assignment.RefOptionType5Answer, '') AS AssignmentCategory5Answer,
 ISNULL(Assignment.RefOptionType6Text,'') AS AssignmentCategory6Question,
 ISNULL(Assignment.RefOptionType6Answer, '') AS AssignmentCategory6Answer,
 Assignment.Ref1,
 Assignment.Ref2,
 Client.AgencyRefExport AS ClientRef,
 Client.Name AS Client,
 ISNULL(TimesheetPurchaseOrder.PurchaseOrderNo, '') AS PurchaseOrderNo,
 TransactionItem.TransactionId,
 TransactionItem.StatusId,
 TransactionItem.[Status],
 TransactionItem.EndDate,
 TransactionItem.StatusChangeDate AS ValueDate,
 TransactionItem.BillRateCodeDescription AS BillRateCodeDescription,
 TransactionItem.BillCode,
 TransactionItem.BillBaseExchangeRateFromCurrency AS BillCurrency,
 TransactionItem.BillBillingUnit AS BillBillingUnit,
 TransactionItem.BillUnits AS BillUnits, 
 TransactionItem.BillDescription AS BillDescription,
 Supplier.SalesTaxTypePercentage AS SupplierVatRate,
 ROUND(TransactionItem.BillRate, 2) AS BillRate,
 ROUND(TransactionItem.BillTotalNetSalesTax, 2) AS BillTotal,
 ROUND(TransactionItem.BillSalesTaxAmt, 2) AS BillVatAmt,
 ISNULL(TransactionItem.BillSalesTaxRate, 0) AS BillVatRate,
 ROUND(TransactionItem.BillTotalGross, 2)  AS BillGrossAmt,
 ISNULL(BillInvoice.InvoiceNo, 0) AS InvoiceRef,
 ISNULL(BillInvoice.InvoiceDate, NULL) AS InvoiceDate,
 DATEADD(Day,CASE WHEN ISNUMERIC(LEFT(Client.PaymentTerms,2)) = 1 THEN CAST(LEFT(Client.PaymentTerms,2) AS DECIMAL)
      WHEN ISNUMERIC(LEFT(Agency.PaymentTerms,2)) = 1 THEN CAST(LEFT(Agency.PaymentTerms,2) AS DECIMAL)
      ELSE 30.0
    END,
      BillInvoice.InvoiceDate) AS InvoiceDueDate, 
 CASE WHEN Assignment.SupplierId = 0 THEN
  'PAYE'
 ELSE
  ISNULL(Supplier.AgencyRefExport,' ') 
 END as SupplierRef, 
 ISNULL(Supplier.Name, '') AS Supplier,
 CASE
  WHEN ISNULL(Supplier.IsSelfBilling, 0) = 0 THEN 'FALSE'
  ELSE 'TRUE'
 END AS SupplierIsSelfBill,
 TransactionItem.PayRateCodeDescription AS PayRateCodeDescription,
 TransactionItem.PayCode,
 TransactionItem.PayBaseExchangeRateFromCurrency AS PayCurrency,
 ROUND(TransactionItem.PayCostOfSaleAmt, 2) AS NiAmt,
 TransactionItem.PayBillingUnit AS PayBillingUnit,
 TransactionItem.PayUnits AS PayUnits,
 TransactionItem.PayDescription AS PayDescription,
 ROUND(TransactionItem.PayRate, 2) AS PayRate,
 ROUND(TransactionItem.PayTotalNetOfSalesTax, 2) AS PayTotal,
 ROUND(TransactionItem.PaySalesTaxAmt, 2) AS PayVatAmt,
 ISNULL(TransactionItem.PaySalesTaxRate, 0) AS PayVatRate,
 ROUND(TransactionItem.PayTotalGross, 2) AS PayGrossAmt,
 ISNULL(PayInvoice.AgencyInvoiceNo, 0) AS PayInvoiceRef,
 ISNULL(PayInvoice.InvoiceDate, '') AS PayInvoiceDate,
 DATEADD(Day,CASE WHEN ISNUMERIC(LEFT(Supplier.PaymentTerms,2)) = 1 THEN CAST(LEFT(Supplier.PaymentTerms,2) AS DECIMAL)
      WHEN ISNUMERIC(LEFT(Agency.PaymentTerms,2)) = 1 THEN CAST(LEFT(Agency.PaymentTerms,2) AS DECIMAL)
      ELSE 30.0
      END,
   TransactionItem.StatusChangeDate) AS PayInvoiceDueDate, 
 ISNULL(PayInvoice.InvoiceDatePaid, '') AS ActualPaymentDate,
 ISNULL(PayInvoice.SupplierId, '') AS LtdCompanyRef,
 Agency.BaseCurrencyName AS AgencyCurrency,
 ExchangeRateBill.Rate AS ExchangeRateBill,
 ExchangeRatePay.Rate AS ExchangeRatePay,
 ((TransactionItem.BillTotalNetSalesTax * ExchangeRateBill.Rate) - ((TransactionItem.PayTotalNetOfSalesTax + TransactionItem.PayCostOfSaleAmt) * ExchangeRatePay.Rate)) AS Margin,
 CASE WHEN TransactionItem.PayTotalNetOfSalesTax > 0 AND TransactionItem.BillTotalNetSalesTax > 0 THEN
  1 - ((TransactionItem.PayTotalNetOfSalesTax + TransactionItem.PayCostOfSaleAmt) * ExchangeRatePay.Rate) / (TransactionItem.BillTotalNetSalesTax * ExchangeRateBill.Rate)    
 ELSE
  0
 END AS MarginPercent
FROM
 @TransactionItemDistinct AS TransactionItem
 INNER JOIN Assignment ON TransactionItem.AssignmentId = Assignment.AssignmentId
 INNER JOIN Agency ON Agency.AgencyId = Assignment.AgencyId
 INNER JOIN Client ON Client.ClientId = Assignment.ClientId
 INNER JOIN Candidate ON Assignment.CandidateId = Candidate.CandidateId
 INNER JOIN ExchangeRate AS ExchangeRateBill ON TransactionItem.BillBaseExchangeRateId = ExchangeRateBill.ExchangeRateId AND TransactionItem.AgencyId = ExchangeRateBill.AgencyId
 INNER JOIN ExchangeRate AS ExchangeRatePay ON TransactionItem.PayBaseExchangeRateId = ExchangeRatePay.ExchangeRateId AND TransactionItem.AgencyId = ExchangeRatePay.AgencyId
 LEFT JOIN BillInvoice ON BillInvoice.InvoiceId = TransactionItem.BillInvoiceId
 LEFT JOIN PurchaseOrder AS TimesheetPurchaseOrder ON TransactionItem.PurchaseOrderId = TimesheetPurchaseOrder.PurchaseOrderId
 LEFT JOIN PayInvoice ON PayInvoice.InvoiceId = TransactionItem.PayInvoiceId
 LEFT JOIN PayInvoice  AS PayInvoiceCommission ON PayInvoiceCommission.InvoiceId = TransactionItem.PayCommissionInvoiceId
 LEFT JOIN AssignmentConsultant AS Consultant ON Assignment.ConsultantId = Consultant.ConsultantId
 LEFT JOIN Supplier ON Assignment.SupplierId = Supplier.SupplierId
 LEFT JOIN Sector ON Sector.SectorId = Assignment.SectorId
 LEFT JOIN Branch ON Branch.BranchId = Assignment.BranchId
ORDER BY TransactionItem.TransactionId
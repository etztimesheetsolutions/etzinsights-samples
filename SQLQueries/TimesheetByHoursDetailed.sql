SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED; SET ANSI_WARNINGS OFF

DECLARE @StatusCSV NVARCHAR(MAX) = '0,1,2,3,4,5,6';
DECLARE @StartDate DATETIME = '2015-01-01';
DECLARE @EndDate DATETIME = '2015-02-01';
DECLARE @ByPeriodEndDate BIT = 0;
DECLARE @RateType INT = 1;

DECLARE @Index INT 
SET @Index = -1 
SET @StatusCSV = RTRIM(LTRIM(@StatusCSV));

DECLARE @Values TABLE ([ID] int IDENTITY(1, 1) PRIMARY KEY, Value NVARCHAR(4000))
WHILE (LEN(@StatusCSV) > 0) BEGIN
 SET @Index = CHARINDEX(',', @StatusCSV)
 IF (@Index = 0) BEGIN  
  INSERT INTO @Values VALUES(LTRIM(@StatusCSV))
  BREAK
 END
 ELSE BEGIN
  INSERT INTO @Values VALUES(LTRIM(LEFT(@StatusCSV, @Index - 1)))
 END
 SET @StatusCSV = RIGHT(@StatusCSV, (LEN(@StatusCSV) - @Index))
END


SELECT
 TransactionItem.TransactionId AS [Timesheet Ref],
 Assignment.Ref1 AS Ref1,
    Assignment.Ref2 AS Ref2,
 TransactionItem.StatusChangeDate AS [Received],
 TransactionItem.EndDate AS [End Date],
 Client.AgencyRefExport AS [Client Reference], 
 Client.Name AS [Client Name],
 ISNULL(Candidate.AgencyRef,'') AS [Candidate Agency Ref],
    ISNULL(Candidate.AgencyRefExport,'') AS [Payroll No],
    Candidate.FirstName AS [First Name], 
    Candidate.LastName AS [Last Name], 
 CASE @RateType
 --RateType = 1 - Pay, RateType = 2 - Bill
  WHEN 1 THEN
   ISNULL(TransactionItem.PayUnits, 0) 
  WHEN 2 THEN
   ISNULL(TransactionItem.BillUnits, 0)
 END AS [Units],
 CASE @RateType
 --RateType = 1 - Pay, RateType = 2 - Bill
  WHEN 1 THEN
   ISNULL(TransactionItem.PayRate, 0)
  WHEN 2 THEN
   ISNULL(TransactionItem.BillRate, 0)
 END AS [Rate],
  CASE @RateType
 --RateType = 1 - Pay, RateType = 2 - Bill
  WHEN 1 THEN
   CASE WHEN LEN(TransactionItem.PayDescription) > 0 THEN
    TransactionItem.PayDescription 
   ELSE
    ISNULL(TransactionItem.PayRateCodeDescription, '')
   END
  WHEN 2 THEN
   CASE WHEN LEN(TransactionItem.BillDescription) > 0 THEN
    TransactionItem.BillDescription
   ELSE
    ISNULL(TransactionItem.BillRateCodeDescription, '')
   END
 END AS [Description],
 CASE @RateType
 --RateType = 1 - Pay, RateType = 2 - Bill
  WHEN 1 THEN
   ISNULL(TransactionItem.PayUnits, 0) * ISNULL(TransactionItem.PayRate, 0)
  WHEN 2 THEN
   ISNULL(TransactionItem.BillUnits, 0) * ISNULL(TransactionItem.BillRate, 0)
 END AS [Total],
 Assignment.PayCurrencyCode AS [Total Currency],
 PayInvoice.InvoiceDate AS [Processed],
 DATENAME(weekday, TimesheetDay.TimesheetDate) AS [Day],
 TimesheetDay.Start,
 TimesheetDay.Finish,
 TimesheetDay.BreakHrs AS [Break Hrs],
 ISNULL(TimesheetDay.TotalHrs, 0) AS [Hours],
 TimesheetDay.Days
FROM
 TransactionItem
 INNER JOIN Assignment ON TransactionItem.AssignmentId = Assignment.AssignmentId
 LEFT JOIN TimesheetDay ON TimesheetDay.TransactionId = TransactionItem.TransactionId
 LEFT JOIN TransactionItemClientAuthoriser AS Authoriser ON TransactionItem.TransactionId = Authoriser.TransactionId 
 INNER JOIN Candidate ON Candidate.CandidateId = Assignment.CandidateId
 INNER JOIN Client ON Assignment.ClientId = Client.ClientId
 LEFT JOIN PayInvoice ON PayInvoice.InvoiceId = TransactionItem.PayInvoiceId
 LEFT JOIN BillInvoice ON BillInvoice.InvoiceId = TransactionItem.BillInvoiceId
WHERE 
 (TransactionItem.StatusId IN (SELECT [Value] FROM @Values))
 AND ((@ByPeriodEndDate = 0 AND (TransactionItem.StatusChangeDate BETWEEN @StartDate AND DATEADD(d, 1, @EndDate)))
 OR (@ByPeriodEndDate = 1 AND (TransactionItem.EndDate BETWEEN @StartDate AND DATEADD(d, 1, @EndDate))))
 AND ((@RateType = 1 AND TransactionItem.PayRateCodeTypeId IN (0, 1, 3, 4, 5, 6, 9, 10))
 OR (@RateType = 2 AND TransactionItem.BillRateCodeTypeId IN (0, 1, 3, 4, 5, 6, 9, 10)))
ORDER BY
 Candidate.LastName, Candidate.FirstName, TransactionItem.EndDate, TransactionItem.TransactionId, CASE WHEN @RateType = 1 THEN TransactionItem.PayRateCodeId WHEN @RateType = 2 THEN TransactionItem.BillRateCodeId END, TimesheetDay.TimesheetDate;
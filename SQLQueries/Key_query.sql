SET NOCOUNT ON

DECLARE @AssignmentType Bit =0;
DECLARE @Branch Bit =0;
DECLARE @Consultant Bit =0;
DECLARE @RateCodeType Bit =0;
DECLARE @RateType Bit =0;
DECLARE @Sector Bit =0;
DECLARE @SortBy Bit =0;
DECLARE @Status Bit =0;

DECLARE @Key TABLE ([Name] NVARCHAR(MAX), [Id] INT, [Description] NVARCHAR(MAX))

IF @AssignmentType = 1
BEGIN
 INSERT INTO @Key
 VALUES ('AssignmentType', 1, 'Temporary'),
     ('AssignmentType', 2, 'Permanent')
END

INSERT INTO @Key
SELECT
 'Branch',
 Branch.BranchId,
 Branch.Description
FROM [dbo].[Branch] AS Branch
WHERE @Branch = 1
ORDER BY Branch.BranchId

INSERT INTO @Key
SELECT
 'Consultant',
 AssignmentConsultant.ConsultantId,
 AssignmentConsultant.LastFirstName
FROM [dbo].[AssignmentConsultant] AS AssignmentConsultant
WHERE @Consultant = 1
ORDER BY AssignmentConsultant.ConsultantId

INSERT INTO @Key
SELECT DISTINCT
 'RateCodeType',
 TransactionItem.[PayRateCodeTypeId],
 TransactionItem.[PayRateCodeTypeName]
FROM [dbo].[TransactionItem] AS TransactionItem
WHERE @RateCodeType = 1
ORDER BY TransactionItem.PayRateCodeTypeId

IF @RateType = 1
BEGIN
 INSERT INTO @Key
 VALUES('RateType', 1, 'Pay'),
 ('RateType', 2, 'Bill')
END

INSERT INTO @Key
SELECT
 'Sector',
 Sector.SectorId,
 Sector.[Description]
FROM [dbo].[Sector] AS Sector
WHERE @Sector = 1
ORDER BY Sector.SectorId

IF @SortBy = 1
BEGIN
 INSERT INTO @Key
 VALUES('SortBy', 0, 'EndDate'),
 ('SortBy', 1, 'ValueDate')
END

INSERT INTO @Key
SELECT DISTINCT
 'StatusId',
 TransactionItem.StatusId,
 TransactionItem.StatusName
FROM [dbo].[TransactionItem] AS TransactionItem
WHERE @Status = 1
ORDER BY TransactionItem.StatusId

SELECT *
FROM @Key
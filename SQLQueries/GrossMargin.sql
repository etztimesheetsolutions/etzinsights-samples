SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
SET ANSI_WARNINGS OFF

DECLARE @AgencyId INT = 410; -- Enter the AgencyId here. See Agency table for a list of available values.
DECLARE @StartDate DATETIME = '2015-01-01';
DECLARE @EndDate DATETIME = '2015-02-01';
DECLARE @ConsultantId INT =  NULL;
DECLARE @ByPeriodEndDate INT = 0;
DECLARE @IncludeRateCodeTypeCSV NVARCHAR(MAX) = ' 0,1,2,3,4,5,6,7,8,9,10,11,12';
DECLARE @CalculateHPWithNI BIT = 0;
DECLARE @IncludeAssignmentTypeCSV NVARCHAR(MAX) = ' 1,2';
DECLARE @IncludeBranchIdCSV NVARCHAR(MAX) = NULL;
DECLARE @IncludeSectorIdCSV NVARCHAR(MAX) = NULL;
DECLARE @NIRate DECIMAL(4, 4) = 0.2;

IF @ConsultantId = 0 SET @ConsultantId = NULL
DECLARE @ShowClientDiscount BIT = CASE WHEN EXISTS (SELECT ClientId FROM Client WHERE Discount <> 0) THEN 1 ELSE 0 END

--Aggregates pay and bill values by TransactionItem lines to sum over each timesheet
DECLARE @TimesheetSummed TABLE (
  TransactionId INT,
        PayUnits DECIMAL(18, 4),
        PayAmt DECIMAL(18, 2),
        PayCostofSaleAmt Decimal(18, 2),
        BillUnits DECIMAL(18, 4),
        BillAmt DECIMAL(18, 4),
        BillCostofSaleAmt Decimal(18, 2),
        SummedValueForHPWithNiValue DECIMAL(18, 4),
        InvoiceAgencyRef NVARCHAR(MAX),
        InvoiceDate DATE)
INSERT INTO @TimesheetSummed (TransactionId, PayUnits, PayAmt, PayCostofSaleAmt, BillUnits, BillAmt, BillCostofSaleAmt, SummedValueForHPWithNiValue, InvoiceAgencyRef, InvoiceDate)
SELECT TransactionItem.[TransactionId],
    SUM(CASE WHEN (@IncludeRateCodeTypeCSV IS NULL OR CHARINDEX(CAST(TransactionItem.[PayRateCodeTypeId] AS VARCHAR(2)), @IncludeRateCodeTypeCSV) > 0) THEN TransactionItem.[PayUnits] ELSE 0 END),
    SUM(CASE WHEN (@IncludeRateCodeTypeCSV IS NULL OR CHARINDEX(CAST(TransactionItem.[PayRateCodeTypeId] AS VARCHAR(2)), @IncludeRateCodeTypeCSV) > 0) THEN TransactionItem.[PayUnits] * TransactionItem.[PayRate] ELSE 0 END),
    SUM(CASE WHEN (@IncludeRateCodeTypeCSV IS NULL OR CHARINDEX(CAST(TransactionItem.[PayRateCodeTypeId] AS VARCHAR(2)), @IncludeRateCodeTypeCSV) > 0) THEN TransactionItem.[PayCostOfSaleAmt] ELSE 0 END),
    SUM(CASE WHEN (@IncludeRateCodeTypeCSV IS NULL OR CHARINDEX(CAST(TransactionItem.[BillRateCodeTypeId] AS VARCHAR(2)), @IncludeRateCodeTypeCSV) > 0) THEN TransactionItem.[BillUnits] ELSE 0 END),
    SUM(CASE WHEN (@IncludeRateCodeTypeCSV IS NULL OR CHARINDEX(CAST(TransactionItem.[BillRateCodeTypeId] AS VARCHAR(2)), @IncludeRateCodeTypeCSV) > 0) THEN TransactionItem.[BillUnits] * TransactionItem.[BillRate] ELSE 0 END),
    SUM(CASE WHEN (@IncludeRateCodeTypeCSV IS NULL OR CHARINDEX(CAST(TransactionItem.[BillRateCodeTypeId] AS VARCHAR(2)), @IncludeRateCodeTypeCSV) > 0) THEN TransactionItem.[BillCostOfSaleAmt] ELSE 0 END),
    SUM(CASE WHEN TransactionItem.[PayRateCodeTypeId] <> 2 THEN (TransactionItem.[PayRate] * TransactionItem.[PayUnits]) ELSE 0 END), --Sums per line before calculating HP
    MAX(BillInvoice.[InvoiceNo]),
    MAX(BillInvoice.[InvoiceDate])
FROM [dbo].[TransactionItem] AS TransactionItem
 LEFT JOIN [dbo].[BillInvoice] AS BillInvoice ON TransactionItem.[BillInvoiceId] = BillInvoice.[InvoiceId]
WHERE ((@ByPeriodEndDate = 1 AND (TransactionItem.[EndDate] BETWEEN @StartDate AND @EndDate))
    OR (@ByPeriodEndDate = 0 AND (TransactionItem.[StatusChangeDate] BETWEEN @StartDate AND DATEADD(d, 1, @EndDate)))
    OR @ByPeriodEndDate = 2 AND (BillInvoice.[InvoiceDate] BETWEEN @StartDate and @EndDate))
    AND TransactionItem.[StatusId] = 2
    AND TransactionItem.AgencyId = @AgencyId
GROUP BY TransactionItem.[TransactionId];

--Combines aggregated pay and bill columns with TransactionItem columns that don't require aggregation
DECLARE @Timesheet TABLE(
 [TimesheetId] INT NOT NULL,
 [AssignmentId] INT NOT NULL,
 [StartDate] SMALLDATETIME NOT NULL,
 [EndDate] SMALLDATETIME NOT NULL,
 [BillUnits] DECIMAL(18, 2) NOT NULL,
 [BillAmt] DECIMAL(18, 2) NOT NULL,
 [PayUnits] DECIMAL(18, 2) NOT NULL,
 [PayAmt] DECIMAL(18, 2) NOT NULL,
 [NiAmt] DECIMAL(18, 2) NOT NULL,
 [InvoiceId] INT,
 [PayInvoiceId] INT,
 [PayRunId] INT,
 [ValueDate] SMALLDATETIME,
 [BillBaseExchangeRate] DECIMAL(18, 2),
 [BillBaseExchangeRateFromCurrency] NVARCHAR(MAX),
 [BillBaseExchangeRateToCurrency] NVARCHAR(MAX),
 [PayBaseExchangeRate] DECIMAL(18, 2),
 [PayBaseExchangeRateFromCurrency] NVARCHAR(MAX),
 [PayBaseExchangeRateToCurrency] NVARCHAR(MAX),
 [PayBillExchangeRate] DECIMAL(18, 2),
 [InvoiceAgencyRef] NVARCHAR(MAX),
 [InvoiceDate] DATETIME,
 [HP] DECIMAL(18, 2))
INSERT INTO @Timesheet ([TimesheetId], [AssignmentId], [StartDate], [EndDate], [BillUnits], [BillAmt], [PayUnits],
      [PayAmt], [NiAmt], [InvoiceId], [PayInvoiceId], [PayRunId], [ValueDate], [BillBaseExchangeRate], [BillBaseExchangeRateFromCurrency],
      [BillBaseExchangeRateToCurrency], [PayBaseExchangeRate], [PayBaseExchangeRateFromCurrency], [PayBaseExchangeRateToCurrency],
      [PayBillExchangeRate], [InvoiceAgencyRef], [InvoiceDate], [HP])
SELECT DISTINCT
  TransactionItem.[TransactionId],
  TransactionItem.[AssignmentId],
  TransactionItem.[StartDate],
  TransactionItem.[EndDate],
  TimesheetSummed.[BillUnits],
  TimesheetSummed.[BillAmt],
  TimesheetSummed.[PayUnits],
  TimesheetSummed.[PayAmt],
  TimesheetSummed.[PayCostofSaleAmt],
  TransactionItem.[BillInvoiceId],
  TransactionItem.[PayInvoiceId],
  TransactionItem.[PayRunId],
  TransactionItem.[StatusChangeDate],
  TransactionItem.[BillBaseExchangeRate],
  TransactionItem.[BillBaseExchangeRateFromCurrency],
  TransactionItem.[BillBaseExchangeRateToCurrency],
  TransactionItem.[PayBaseExchangeRate],
  TransactionItem.[PayBaseExchangeRateFromCurrency],
  TransactionItem.[PayBaseExchangeRateToCurrency],
  TransactionItem.[PayBillBaseExchangeRate],
  TimesheetSummed.[InvoiceAgencyRef],
  TimesheetSummed.[InvoiceDate],
  CASE WHEN @CalculateHPWithNI = 0 THEN
 CASE 
  WHEN Assignment.[SupplierId] IS NOT NULL AND TransactionItem.[EndDate] < '01-OCT-2007' THEN TimesheetSummed.[PayAmt] * .0833
  WHEN Assignment.[SupplierId] IS NOT NULL AND TransactionItem.[StartDate] >= '01-OCT-2007' AND TransactionItem.[StartDate] < '01-APR-2009' THEN TimesheetSummed.[PayAmt] * .1017
  WHEN Assignment.[SupplierId] IS NOT NULL AND TransactionItem.[StartDate] >= '01-APR-2009' THEN TimesheetSummed.[PayAmt] * .1207
  ELSE 0
 END
  ELSE
 CASE 
  WHEN Assignment.[SupplierId] IS NOT NULL AND TransactionItem.[EndDate] < '01-OCT-2007' THEN (TimesheetSummed.[PayAmt] * .0833) * (1 + @NIRate)
  WHEN Assignment.[SupplierId] IS NOT NULL AND TransactionItem.[StartDate] >= '01-OCT-2007' AND TransactionItem.[StartDate] < '01-APR-2009' THEN (TimesheetSummed.[PayAmt] * .1017) * (1 + @NIRate)
  WHEN Assignment.[SupplierId] IS NOT NULL AND TransactionItem.[StartDate] >= '01-APR-2009' THEN (TimesheetSummed.[PayAmt] * .1207) * (1 + @NIRate)
  ELSE 0
 END
  END AS [HP]
FROM [dbo].[TransactionItem]
 INNER JOIN @TimesheetSummed AS TimesheetSummed ON TransactionItem.[TransactionId] = TimesheetSummed.[TransactionId]
 INNER JOIN Assignment ON TransactionItem.AssignmentId = Assignment.AssignmentId

SELECT
 Client.[Name] AS [Client],
 Timesheet.[ValueDate] AS [Date Received],
 ISNULL(Timesheet.[InvoiceDate], '') AS [Invoice Date],
 Timesheet.[TimesheetId] AS [Ref],
 Candidate.[LastFirstName] AS [Candidate],
 COALESCE(AssignmentConsultant.[LastFirstName], 'Unassigned') AS [Consultant],
 Timesheet.[EndDate] AS [Period Ending Date],
 Timesheet.[BillUnits] AS [Units],
 ISNULL(Client.[Discount], 0) AS [Discount %],
 Timesheet.[BillBaseExchangeRateToCurrency] AS [Agency Currency Code],
 (Timesheet.[BillAmt] + (Client.[ChargeEmployersTax] * Timesheet.[NiAmt])) * Timesheet.[BillBaseExchangeRate] AS [Agency Currency Bill Amt],
 (Timesheet.[PayAmt] * Timesheet.[PayBaseExchangeRate]) AS [Agency Currency Pay Amt],
 ISNULL(Client.[Discount], 0) * ((Timesheet.[BillAmt] + (Client.[ChargeEmployersTax] * Timesheet.[NiAmt]))) * Timesheet.[BillBaseExchangeRate] AS [Agency Currency Discount Amt],
 Timesheet.[BillBaseExchangeRateFromCurrency] AS [Timesheet Currency Code],
 Timesheet.[BillAmt] + (Client.[ChargeEmployersTax] * Timesheet.[NiAmt]) AS [Bill Amt],
 Timesheet.[PayAmt] AS [Pay Amt],
 ISNULL(Client.[Discount], 0) * ((Timesheet.[BillAmt] + (Client.[ChargeEmployersTax] * Timesheet.[NiAmt]))) AS [Discount Amt],
 Timesheet.[NiAmt] AS [Super / NI Amt],
 Timesheet.[HP],
 (Timesheet.[BillAmt] + (Client.[ChargeEmployersTax] * Timesheet.[NiAmt])) * (1 - ISNULL(Client.[Discount], 0)) - (Timesheet.[PayAmt] + Timesheet.[NiAmt] + Timesheet.[HP]) AS [Gross Margin],
 (Timesheet.[BillAmt] + (Client.[ChargeEmployersTax] * Timesheet.[NiAmt])) * (1 - ISNULL(Client.[Discount], 0)) * Timesheet.[BillBaseExchangeRate] - (Timesheet.[PayAmt] + Timesheet.[NiAmt] + Timesheet.[HP]) * Timesheet.[PayBaseExchangeRate] AS [Agency Currency Gross], 
 CASE WHEN Timesheet.[BillAmt] = 0 THEN
  0.00
 ELSE
  ((Timesheet.[BillAmt] + (Client.[ChargeEmployersTax] * Timesheet.[NiAmt])) * (1 - ISNULL(Client.[Discount], 0)) - (Timesheet.[PayAmt] + Timesheet.[NiAmt] + Timesheet.[HP]))/ (Timesheet.[BillAmt] + (Client.[ChargeEmployersTax] * Timesheet.[NiAmt]))
 END AS [GM %],
 ISNULL(Timesheet.InvoiceAgencyRef, '') AS [Invoice],
 Assignment.Ref1,
 ISNULL(Branch.[Description], 'Unassigned') AS [Branch],
 ISNULL(Sector.[Description], 'Unassigned') AS [Sector],
 Assignment.[StartDate] AS [Start Date]
FROM
 @Timesheet AS Timesheet
 INNER JOIN Assignment ON Timesheet.[AssignmentId] = Assignment.[AssignmentId]
 INNER JOIN Client ON Assignment.ClientId = Client.ClientId
 INNER JOIN Candidate ON Assignment.[CandidateId] = Candidate.[CandidateId]
 LEFT JOIN AssignmentConsultant ON Assignment.[ConsultantId] = AssignmentConsultant.[ConsultantId]
 LEFT JOIN Branch ON Assignment.[BranchId] = Branch.[BranchId]
 LEFT JOIN Sector ON Assignment.[SectorId] = Sector.[SectorId]
WHERE
 (@ConsultantId IS NULL OR CHARINDEX(CAST(Assignment.[ConsultantId] AS VARCHAR(6)), @ConsultantId) > 0)
 AND (Timesheet.[PayAmt] != 0 OR Timesheet.[BillAmt] != 0)
 AND (@IncludeSectorIdCSV IS NULL OR CHARINDEX(CAST(Assignment.[SectorId] AS VARCHAR(6)), @IncludeSectorIdCSV) > 0)
 AND (@IncludeBranchIdCSV IS NULL OR CHARINDEX(CAST(Assignment.[BranchId] AS VARCHAR(6)), @IncludeBranchIdCSV) > 0)
 AND (@IncludeAssignmentTypeCSV IS NULL OR CHARINDEX(CAST(Assignment.[AssignmentType] AS VARCHAR(1)), @IncludeAssignmentTypeCSV) > 0)
ORDER BY
 Client.Name, Candidate.LastFirstName
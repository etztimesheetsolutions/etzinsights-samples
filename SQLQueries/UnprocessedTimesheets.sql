SET NOCOUNT ON;
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
SET ANSI_WARNINGS OFF;

DECLARE @PayRunId VARCHAR(MAX) = '0';
DECLARE @PayUnBatched BIT = 0;
DECLARE @BillUnbatched BIT = 0;
DECLARE @StatusCSV NVARCHAR(4000) = '2';
DECLARE @ShowConsultantCommission BIT = 0;

SELECT
 TransactionItem.PayRunId AS [Pay Run Batch Id],
 Assignment.AgencyRef AS [Assignment Agency Ref],
 TransactionItem.TransactionId AS [Timesheet Ref],
 TransactionItem.EndDate AS [End Date],
 TransactionItem.StatusChangeDate AS [Received Date],
 TransactionItem.StatusName AS [Status],
 COALESCE(Candidate.AgencyRefExport, Candidate.AgencyRef) AS [Candidate Ref],
 Candidate.FullName AS [Candidate],
 Assignment.BillingNotes AS [Costing Notes],
 COALESCE(AssignmentConsultantSplit.FullName, AssignmentConsultant.FullName, 'Unassigned') AS [Consultant],
 COALESCE(ConsultantBranch.[Description], AssignmentBranch.[Description], 'Unassigned') AS [Branch],
 ISNULL(Sector.[Description], '') AS [Sector],
 Assignment.RefOptionType4Answer AS [Country],
 Assignment.Ref1 AS [Assignment Ref1],
 Assignment.Ref2 AS [Assignment Ref2],
 COALESCE(Client.AgencyRefExport, Client.AgencyRef) AS [Client Ref],
 Client.Name AS [Client],
 TransactionItem.PurchaseOrderNo AS [Purchase Order No],
 TransactionItem.BillRateCodeDescription AS [Bill Rate Code Description],
 ISNULL(BillInvoice.InvoiceId, 0) AS [Invoice Ref],
 ISNULL(BillInvoice.InvoiceDate, '') As [Invoice Date],
 CASE WHEN CHARINDEX('days', LOWER(Client.PaymentTerms)) > 0 THEN
  DATEADD(day, CAST(REPLACE(Client.PaymentTerms, 'days', '') AS INT), BillInvoice.InvoiceDate)
 ELSE 
  ''
 END AS [Invoice Due Date],
 TransactionItem.BillCode AS [Bill Code],
 TransactionItem.BillBaseExchangeRateFromCurrency AS [Bill Currency],
 TransactionItem.BillBillingUnitName AS [Bill Billing Unit],
 TransactionItem.BillUnits AS [Bill Units],
 TransactionItem.BillRate * AssignmentConsultantSplit.Split AS [Bill Rate],
 TransactionItem.BillTotalNetSalesTax * AssignmentConsultantSplit.Split AS [Bill Total],
 TransactionItem.BillSalesTaxAmt * AssignmentConsultantSplit.Split AS [Bill Vat],
 TransactionItem.BillTotalGross * AssignmentConsultantSplit.Split AS [Bill Gross],
 TransactionItem.BillSalesTaxRate AS [Bill Vat Rate],
 Supplier.AgencyRef AS [Supplier Ref],
 Supplier.Name AS [Supplier],
 Supplier.SalesTaxTypePercentage AS [Supplier Vat Rate],
 CASE WHEN Supplier.IsSelfBilling = 0 THEN
  'FALSE'
 ELSE
  'TRUE'
 END AS [Self Bill],
 TransactionItem.PayRateCodeDescription AS [Pay Rate Code Description],
 ISNULL(PayInvoice.InvoiceId, 0) AS [Pay Invoice Ref],
 ISNULL(PayInvoice.InvoiceDate, '') As [Pay Invoice Date],
 CASE WHEN CHARINDEX('days', LOWER(Supplier.PaymentTerms)) > 0 THEN
  DATEADD(day, CAST(REPLACE(Supplier.PaymentTerms, 'days', '') AS INT), PayInvoice.InvoiceDate)
 ELSE 
  ''
 END AS [Pay Invoice Due Date],
 ISNULL(PayInvoice.InvoiceDatePaid, '') AS [Pay Invoice Payment Date],
 ISNULL(PayInvoice.SupplierInvoiceNo, '') AS [Supplier PayInvoice Ref],
 TransactionItem.PayCode AS [Pay Code],
 TransactionItem.PayBaseExchangeRateFromCurrency AS [Pay Currency],
 TransactionItem.PayCostOfSaleAmt AS [Super / NI Amt],
 TransactionItem.PayBillingUnit AS [Pay Billing Unit],
 TransactionItem.PayUnits AS [Pay Units],
 TransactionItem.PayRate * AssignmentConsultantSplit.Split AS [Pay Rate],
 TransactionItem.PayTotalNetOfSalesTax * AssignmentConsultantSplit.Split AS [Pay Total],
 TransactionItem.PaySalesTaxAmt * AssignmentConsultantSplit.Split AS [Pay Vat],
 TransactionItem.PayTotalGross * AssignmentConsultantSplit.Split AS [Pay Gross],
 TransactionItem.PaySalesTaxRate AS [Pay Vat Rate],
 (TransactionItem.BillTotalGross - TransactionItem.PayTotalGross) * AssignmentConsultantSplit.Split AS [Margin Amount],
 CASE WHEN TransactionItem.BillTotalGross IS NULL OR TransactionItem.BillTotalGross = 0.0 THEN
  0
 ELSE
  (TransactionItem.BillTotalGross - TransactionItem.PayTotalGross)/TransactionItem.BillTotalGross * AssignmentConsultantSplit.Split
 END AS [Margin %],
 AssignmentConsultantSplit.Split AS [Consultant Split]
FROM
 TransactionItem
 INNER JOIN Assignment ON TransactionItem.AssignmentId = Assignment.AssignmentId
 INNER JOIN Candidate ON Assignment.CandidateId = Candidate.CandidateId
 INNER JOIN Client ON Assignment.ClientId = Client.ClientId
 LEFT JOIN AssignmentConsultant ON Assignment.ConsultantId = AssignmentConsultant.ConsultantId
 LEFT JOIN AssignmentConsultantSplit ON Assignment.AssignmentId = AssignmentConsultantSplit.AssignmentId
 LEFT JOIN Branch AS AssignmentBranch ON Assignment.BranchId = AssignmentBranch.BranchId
 LEFT JOIN Branch AS ConsultantBranch ON AssignmentConsultantSplit.BranchId = ConsultantBranch.BranchId
 LEFT JOIN Sector ON Assignment.SectorId = Sector.SectorId
 LEFT JOIN BillInvoice ON TransactionItem.BillInvoiceId = BillInvoice.InvoiceId
 LEFT JOIN PayInvoice ON TransactionItem.PayInvoiceId = PayInvoice.InvoiceId
 LEFT JOIN Supplier ON Assignment.SupplierId = Supplier.SupplierId
WHERE
 (TransactionItem.PayRunId = 0 OR TransactionItem.PayRunId IS NULL)
 AND (((PayInvoice.PayRunId IS NULL OR PayInvoice.PayRunId = 0) AND TransactionItem.PayTotalGross <> 0)
   OR ((BillInvoice.PayRunId IS NULL OR BillInvoice.PayRunId = 0) AND TransactionItem.BillInvoiceId >= 0 AND TransactionItem.BillTotalGross <> 0))
 AND TransactionItem.StatusId IN (1, 2, 3, 5, 6)
ORDER BY Candidate.LastName, TransactionItem.EndDate, TransactionItem.TransactionId
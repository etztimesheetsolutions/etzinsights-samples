SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
SET ANSI_WARNINGS OFF

DECLARE @StartDate DATETIME = '2015-01-01';
DECLARE @EndDate DATETIME = '2015-02-01';
DECLARE @ByPeriodEndDate INT = 0;
DECLARE @ExcludeExpenseUnits BIT = 0;
DECLARE @CalculateHPWithNI BIT = 0;
DECLARE @IncludeAssignmentTypeCSV NVARCHAR(MAX) = NULL;
DECLARE @ConsultantIdCSV NVARCHAR(MAX) = NULL;
DECLARE @ConsultantId INT = 0;
DECLARE @NIRate DECIMAL(4, 4) = 0;

--Aggregates pay and bill values for TransactionItems into a single Timesheet
DECLARE @TimesheetSummed TABLE (TransactionId INT,
        PayUnits DECIMAL(18, 4),
        PayAmt DECIMAL(18, 2),
        PayCostofSaleAmt DECIMAL(18, 2),
        BillUnits DECIMAL(18, 4),
        BillAmt DECIMAL(18, 4),
        BillCostofSaleAmt DECIMAL(18, 2),
        NiSplitBetweenCosts_NIRate DECIMAL(18, 4),
        SummedValueForHPWithNiValue DECIMAL(18, 4))
INSERT INTO @TimesheetSummed (TransactionId, PayUnits, PayAmt, PayCostofSaleAmt, BillUnits, BillAmt, BillCostofSaleAmt, NiSplitBetweenCosts_NIRate, SummedValueForHPWithNiValue)
SELECT TransactionItem.[TransactionId],
    SUM(CASE WHEN (@ExcludeExpenseUnits = 0 OR TransactionItem.[PayRateCodeTypeId] <> 2) THEN TransactionItem.[PayUnits] ELSE 0 END),
    SUM(CASE WHEN (@ExcludeExpenseUnits = 0 OR TransactionItem.[PayRateCodeTypeId] <> 2) THEN TransactionItem.[PayUnits] * TransactionItem.[PayRate] ELSE 0 END),
    SUM(CASE WHEN (@ExcludeExpenseUnits = 0 OR TransactionItem.[PayRateCodeTypeId] <> 2) THEN TransactionItem.[PayCostOfSaleAmt] ELSE 0 END),
    SUM(CASE WHEN (@ExcludeExpenseUnits = 0 OR TransactionItem.[BillRateCodeTypeId] <> 2) THEN TransactionItem.[BillUnits] ELSE 0 END),
    SUM(CASE WHEN (@ExcludeExpenseUnits = 0 OR TransactionItem.[BillRateCodeTypeId] <> 2) THEN TransactionItem.[BillUnits] * TransactionItem.[BillRate] ELSE 0 END),
    SUM(CASE WHEN (@ExcludeExpenseUnits = 0 OR TransactionItem.[BillRateCodeTypeId] <> 2) THEN TransactionItem.[BillCostOfSaleAmt] ELSE 0 END),
    @NIRate / NULLIF(COUNT(CASE WHEN TransactionItem.[PayUnits] > 0 AND (@ExcludeExpenseUnits = 0 OR TransactionItem.[PayRateCodeTypeId] <> 2) THEN 1 ELSE 0 END), 0),
    SUM(CASE WHEN TransactionItem.[PayRateCodeTypeId] <> 2 THEN (TransactionItem.[PayRate] * TransactionItem.[PayUnits]) ELSE 0 END)
FROM [dbo].[TransactionItem] AS TransactionItem
LEFT JOIN [dbo].[BillInvoice] AS BillInvoice ON TransactionItem.[BillInvoiceId] = BillInvoice.[InvoiceId]
WHERE
 ((@ByPeriodEndDate = 1 AND (TransactionItem.[EndDate] BETWEEN @StartDate AND @EndDate))
  OR (@ByPeriodEndDate = 0 AND (TransactionItem.[StatusChangeDate] BETWEEN @StartDate AND DATEADD(d, 1, @EndDate)))
  OR @ByPeriodEndDate = 2 AND (BillInvoice.[InvoiceDate] BETWEEN @StartDate and @EndDate))
 AND TransactionItem.[StatusId] = 2
GROUP BY TransactionItem.[TransactionId];

--Combines aggregate columns with TransactionItem columns that don't require aggregation
DECLARE @Timesheet TABLE (TimesheetId INT,
       AssignmentId INT,
       EndDate DATETIME,
       StatusChangeDate DATETIME,
       PayUnits DECIMAL(18, 2),
       PayAmt DECIMAL(18, 2),
       PayBaseExchangeRate DECIMAL(18, 4),
       PayBaseExchangeRateID INT,
       PayBaseExchangeRateFromCurrency VARCHAR(3),
    PayBaseExchangeRateToCurrency VARCHAR(3),
       PayCostofSaleAmt DECIMAL(18, 2),
       BillUnits DECIMAL(18, 2),
       BillAmt DECIMAL(18, 2),
       BillBaseExchangeRate DECIMAL(18, 4),
       BillBaseExchangeRateID INT,
       BillBaseExchangeRateFromCurrency VARCHAR(3),
    BillBaseExchangeRateToCurrency VARCHAR(3),
       BillCostofSaleAmt DECIMAL(18, 2),
       StatusId INT,
       BillInvoiceId INT,
       CalculateHPWithNI DECIMAL(18, 4))
INSERT INTO @Timesheet (TimesheetId,
      AssignmentId,
      EndDate,
      StatusChangeDate,
      PayUnits,
      PayAmt,
      PayBaseExchangeRate,
      PayBaseExchangeRateID,
      PayBaseExchangeRateFromCurrency,
   PayBaseExchangeRateToCurrency,
      PayCostofSaleAmt,
      BillUnits,
      BillAmt,
      BillBaseExchangeRate,
      BillBaseExchangeRateID,
      BillBaseExchangeRateFromCurrency,
   BillBaseExchangeRateToCurrency,
      BillCostofSaleAmt,
      StatusId,
      BillInvoiceId,
      CalculateHPWithNI)
SELECT DISTINCT TransactionItem.[TransactionId],
    TransactionItem.[AssignmentId],
    TransactionItem.[EndDate],
    TransactionItem.[StatusChangeDate],
    TimesheetSummed.[PayUnits],
    TimesheetSummed.[PayAmt],
    TransactionItem.[PayBaseExchangeRate],
    TransactionItem.[PayBaseExchangeRateId],
    TransactionItem.[PayBaseExchangeRateFromCurrency],
 TransactionItem.[PayBaseExchangeRateToCurrency],
    TimesheetSummed.[PayCostOfSaleAmt],
    TimesheetSummed.[BillUnits],
    TimesheetSummed.[BillAmt],
    TransactionItem.[BillBaseExchangeRate],
    TransactionItem.[BillBaseExchangeRateID],
    TransactionItem.[BillBaseExchangeRateFromCurrency],
 TransactionItem.[BillBaseExchangeRateToCurrency],
    TimesheetSummed.[BillCostOfSaleAmt],
    TransactionItem.[StatusId],
    TransactionItem.[BillInvoiceId],
    CASE WHEN @CalculateHPWithNI = 0 THEN 0
 ELSE
  CASE 
   WHEN Assignment.[SupplierId] IS NOT NULL AND TransactionItem.[EndDate] < '01-OCT-2007' THEN (TimesheetSummed.[PayAmt] * .0833) * (1 + @NIRate)
   WHEN Assignment.[SupplierId] IS NOT NULL AND TransactionItem.[StartDate] >= '01-OCT-2007' AND TransactionItem.[StartDate] < '01-APR-2009' THEN (TimesheetSummed.[PayAmt] * .1017) * (1 + @NIRate)
   WHEN Assignment.[SupplierId] IS NOT NULL AND TransactionItem.[StartDate] >= '01-APR-2009' THEN (TimesheetSummed.[PayAmt] * .1207) * (1 + @NIRate)
   ELSE 0
  END
 END
FROM @TimesheetSummed AS TimesheetSummed
 INNER JOIN [dbo].[TransactionItem] AS TransactionItem ON TimesheetSummed.[TransactionId] = TransactionItem.[TransactionId]
 INNER JOIN [dbo].[Assignment] AS Assignment ON TransactionItem.[AssignmentId] = Assignment.[AssignmentId];

SELECT
 COALESCE(Branch.[Description], BaseBranch.[Description], 'Unassigned') AS [Branch],
 ISNULL(Sector.[Description], 'Unassigned') AS [Sector],
 COALESCE(ConsultantSplit.[LastFirstName], BaseConsultant.[LastFirstName], 'Unassigned') AS [Consultant],
 ISNULL(BaseConsultant.[LastFirstName], 'Unassigned') AS [Primary Consultant],
 ISNULL(SupportingConsultant.[LastFirstName], 'Unassigned') AS [Supporting Consultant],
 ISNULL(AccountingDirector.[LastFirstName], 'Unassigned') AS [Accounting Director],
 Assignment.Ref1 AS [Reference 1],
 Assignment.Ref2 AS [Reference 2],
 Client.[Name] AS [Client],
 Assignment.[StartDate] AS [Assignment Start Date], 
 Assignment.[EndDate] AS [Assignment End Date],
 ISNULL(BillInvoice.[InvoiceNo], '') AS [Invoice],
 ISNULL(BillInvoice.[InvoiceDate], '') AS [Invoice Raised],
 Candidate.[LastFirstName] AS [Candidate],
 Timesheet.[TimesheetId] AS [Timesheet],
 Timesheet.[EndDate] AS [Period End],
 Timesheet.[StatusChangeDate] AS [Received],
 CASE 
  WHEN Timesheet.[BillUnits] != 0 THEN 
   Timesheet.[BillUnits]
  ELSE 
   Timesheet.[PayUnits]
 END AS [Units],
 Timesheet.[BillBaseExchangeRateToCurrency] AS [Agency Bill Currency],
 Timesheet.[BillAmt] * Timesheet.[BillBaseExchangeRate] AS [Agency Currency Bill Amt],
 Timesheet.[BillBaseExchangeRateFromCurrency] AS [Timesheet Bill Currency],
 Timesheet.[BillAmt] AS [Bill Amt],
 Timesheet.[PayBaseExchangeRateToCurrency] AS [Agency Pay Currency],
 (Timesheet.[PayAmt] + Timesheet.[CalculateHPWithNI]) * Timesheet.[PayBaseExchangeRate] AS [Agency Currency Pay Amt],
 Timesheet.[PayBaseExchangeRateFromCurrency] AS [Timesheet Pay Currency],
 Timesheet.[PayAmt] + Timesheet.[CalculateHPWithNI] AS [Pay Amt],
 (Timesheet.[BillAmt] * Timesheet.[BillBaseExchangeRate]) - (Timesheet.[PayAmt] + Timesheet.[CalculateHPWithNI]) * Timesheet.[PayBaseExchangeRate] AS [Agency Currency Margin],
 Timesheet.[BillAmt] - (Timesheet.[PayAmt] + Timesheet.[CalculateHPWithNI]) AS [Margin],
 ISNULL(ConsultantSplit.[Split], 1) AS [Split],
 ISNULL(ConsultantSplit.[Split], 1) * ((Timesheet.[BillAmt] * Timesheet.[BillBaseExchangeRate]) - ((Timesheet.[PayAmt] + Timesheet.[CalculateHPWithNI]) * Timesheet.PayBaseExchangeRate)) AS [Agency Currency Split Margin], 
 ISNULL(ConsultantSplit.[Split], 1) * (Timesheet.[BillAmt] - (Timesheet.[PayAmt] + Timesheet.[CalculateHPWithNI])) AS [SplitMargin]
FROM
 @Timesheet AS Timesheet
 INNER JOIN Assignment ON Timesheet.[AssignmentId] = Assignment.[AssignmentId]
 INNER JOIN Client ON Assignment.[ClientId] = Client.[ClientId] 
 INNER JOIN Candidate ON Assignment.[CandidateId] = Candidate.[CandidateId]
 INNER JOIN Agency ON Assignment.[AgencyId] = Agency.[AgencyId]
 LEFT JOIN AssignmentConsultantSplit AS ConsultantSplit ON Assignment.[AssignmentId] = ConsultantSplit.[AssignmentId] 
 LEFT JOIN AssignmentConsultant AS BaseConsultant ON Assignment.[ConsultantId] = BaseConsultant.[ConsultantId]
 LEFT JOIN AssignmentConsultant AS SupportingConsultant ON Assignment.[ConsultantSupportingId] = SupportingConsultant.[ConsultantId]
 LEFT JOIN AssignmentConsultant AS AccountingDirector ON Assignment.[AccountDirectorId] = AccountingDirector.[ConsultantId]
 LEFT JOIN Branch ON ConsultantSplit.[BranchId] = Branch.[BranchId]
 LEFT JOIN Branch AS BaseBranch ON Assignment.[BranchId] = BaseBranch.[BranchId]
 LEFT JOIN Sector ON Assignment.[SectorId] = Sector.[SectorId]
 LEFT JOIN BillInvoice ON Timesheet.[BillInvoiceId] = BillInvoice.[InvoiceId]
WHERE
    (Timesheet.[BillUnits] != 0 OR Timesheet.[PayUnits] != 0)
 AND (@IncludeAssignmentTypeCSV IS NULL 
  OR (@IncludeAssignmentTypeCSV = '1' AND Assignment.AssignmentType = 1) 
  OR (@IncludeAssignmentTypeCSV = '2' AND Assignment.AssignmentType = 2) 
  OR (@IncludeAssignmentTypeCSV = '1,2' AND Assignment.AssignmentType IN (1,2)))
   AND ((@ConsultantIdCSV IS NULL AND @ConsultantId = 0) 
       OR (CHARINDEX(CAST(BaseConsultant.ConsultantId AS NVARCHAR(MAX)), @ConsultantIdCSV) > 0)
    OR CAST(BaseConsultant.ConsultantId AS NVARCHAR(MAX)) = @ConsultantId
    OR (CHARINDEX(CAST(SupportingConsultant.ConsultantId AS NVARCHAR(MAX)), @ConsultantIdCSV) > 0)
    OR CAST(SupportingConsultant.ConsultantId AS NVARCHAR(MAX)) = @ConsultantId
    OR (CHARINDEX(CAST(AccountingDirector.ConsultantId AS NVARCHAR(MAX)), @ConsultantIdCSV) > 0)
    OR CAST(AccountingDirector.ConsultantId AS NVARCHAR(MAX)) = @ConsultantId)
ORDER BY
 Timesheet.[TimesheetId], ConsultantSplit.[AssignmentConsultantSplitId]

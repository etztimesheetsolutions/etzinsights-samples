SET NOCOUNT ON;
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
SET ANSI_WARNINGS OFF;

DECLARE @ConsultantId INT = 0;
DECLARE @HoursInDay DECIMAL(18, 2) = 7.5;

IF @ConsultantId = 0 SET @ConsultantId = NULL

DECLARE @StartDate DATETIME,
  @EndDate DATETIME,
  @CurrentDate DATETIME

SELECT 
 @StartDate = MIN(TransactionItem.StartDate),
 @EndDate = MAX(TransactionItem.EndDate),
 @CurrentDate = MIN(TransactionItem.StartDate)
FROM 
 TransactionItem
WHERE 
 TransactionItem.StatusId IN (0,1,3,5)

-- Calendar table to determine weekdays between two periods
DECLARE @Calendar TABLE([Date] DATETIME, WorkingDays INT, WorkingHours DECIMAL(18,2))
WHILE @CurrentDate <= @EndDate BEGIN
 INSERT INTO @Calendar
  VALUES (@CurrentDate, 1, @HoursInDay)
  SET @CurrentDate = DATEADD(d, 1, @CurrentDate)
END

UPDATE @Calendar
SET WorkingDays = 0, WorkingHours = 0 
WHERE DATEPART(dw, Date) IN (1,7)

-- Work days and hours for each timesheet
DECLARE @Timesheet TABLE (TimesheetID INT, WorkingDays INT, WorkingHours INT)
INSERT into @Timesheet
SELECT 
 TransactionId, 
 (SELECT SUM(Calendar.WorkingDays) FROM @Calendar AS Calendar WHERE Calendar.[Date] BETWEEN TransactionItem.StartDate AND TransactionItem.EndDate) AS WorkingDays,
 (SELECT SUM(Calendar.WorkingHours) FROM @Calendar AS Calendar WHERE Calendar.[Date] BETWEEN TransactionItem.StartDate AND TransactionItem.EndDate) AS WorkingHours
FROM
 TransactionItem
WHERE
 TransactionItem.StatusId IN (0,1,3,5)
 AND GETDATE() > TransactionItem.EndDate

DECLARE @AssignmentRate TABLE (AssignmentID INT, PayRate DECIMAL(18, 2), BillRate DECIMAL(18, 2))
INSERT INTO @AssignmentRate
SELECT
 TransactionItem.AssignmentId,
 MIN(AssignmentRate.PayRate) AS PayRate,
 MIN(AssignmentRate.BillRate) AS BillRate
FROM
 TransactionItem
 INNER JOIN AssignmentRate On AssignmentRate.AssignmentId = TransactionItem.AssignmentId
WHERE 
 TransactionItem.StatusId IN (0,1,3,5) 
 AND (TransactionItem.BillRateCodeTypeId NOT IN (7, 8) OR TransactionItem.PayRateCodeTypeId NOT IN (7, 8)) --Equiv of AssignmentRateDay.RateCategory = 1. BillRateCodeTypeId = 7&8 refers to allowances
 AND (AssignmentRate.PayRate > 0 AND AssignmentRate.BillRate > 0)
GROUP BY
 TransactionItem.AssignmentId

SELECT
 TransactionItem.TransactionId AS Ref,
 Candidate.LastFirstName AS Candidate,
 Client.Name AS Client,
 Consultant.LastFirstName AS Consultant,
 ISNULL(Branch.Description, '') AS Branch,
 ISNULL(Sector.Description, '') AS Sector,
 Assignment.Ref1 AS [Assignment Ref 1],
 Assignment.Ref2 AS [Assignment Ref 2],
 Supplier.Name AS Supplier,
 TransactionItem.StartDate AS Start,
 TransactionItem.EndDate AS [End],
 TransactionItem.StatusName AS [Status],
 Timesheet.WorkingDays AS [Days],
 Timesheet.WorkingHours AS [Hours],
 -------Pay Details------------
 CASE WHEN Assignment.PayBillingUnit = 2 THEN
  'Daily'
 ELSE
  'Hourly'
 END AS [Pay Unit],
 AssignmentRate.PayRate AS [Pay Rate],
 CASE WHEN Assignment.PayBillingUnit = 2 THEN
  Timesheet.WorkingDays * AssignmentRate.PayRate
 ELSE
  Timesheet.WorkingHours * AssignmentRate.PayRate
 END AS [Pay Total],
 TransactionItem.PayBaseExchangeRateFromCurrency AS [PayCurrency],
 -------Bill Details----------
 CASE WHEN Assignment.BillBillingUnit = 2 THEN
  'Daily'
 ELSE
  'Hourly'
 END AS [Bill Unit],
 AssignmentRate.BillRate AS [Bill Rate],
 CASE WHEN Assignment.BillBillingUnit = 2 THEN
  Timesheet.WorkingDays * AssignmentRate.BillRate
 ELSE
  Timesheet.WorkingHours * AssignmentRate.BillRate
 END AS [Bill Total],
 TransactionItem.BillBaseExchangeRateFromCurrency AS [Bill Currency]
FROM
 TransactionItem
 LEFT JOIN @Timesheet as Timesheet ON TransactionItem.TransactionId = Timesheet.TimesheetId
 INNER JOIN Assignment ON Assignment.AssignmentId = TransactionItem.AssignmentId
 INNER JOIN Candidate ON Candidate.CandidateId = Assignment.CandidateId
 LEFT JOIN AssignmentConsultant AS Consultant ON Consultant.ConsultantId = Assignment.ConsultantId
 LEFT JOIN Supplier ON Supplier.SupplierId = Assignment.SupplierId
 INNER JOIN Client ON Client.ClientId = Assignment.ClientId
 LEFT JOIN @AssignmentRate AS AssignmentRate ON Assignment.AssignmentId = AssignmentRate.AssignmentId
 LEFT JOIN Sector ON Assignment.SectorId = Sector.SectorId
 LEFT JOIN Branch ON Assignment.BranchId = Branch.BranchId
WHERE
 TransactionItem.StatusId IN (0,1,3,5)
 AND GETDATE() > TransactionItem.EndDate
 AND (@ConsultantId IS NULL OR Assignment.ConsultantId = @ConsultantId OR Assignment.AccountDirectorId = @ConsultantId OR Assignment.ConsultantSupportingId = @ConsultantId)
ORDER BY
 Candidate.LastFirstName
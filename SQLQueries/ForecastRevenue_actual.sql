SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
SET ANSI_WARNINGS OFF

DECLARE @StartOfYear INT = 2014;
DECLARE @EndOfYear INT = 2014;
DECLARE @DayHourConversion DECIMAL(2, 1) = 7.5;
DECLARE @ByPeriodEndDate INT = 1;
DECLARE @ExcludeExpenseUnits INT = 0;
DECLARE @ConsultantId INT = 0;

DECLARE @StartDate DATETIME
DECLARE @EndDate DATETIME
IF @ConsultantId = 0 SET @ConsultantId = NULL
SET @StartDate = CAST(CAST(@StartOfYear AS NVARCHAR(8)) + '-01-01' AS DATETIME)
SET @EndDate = CAST(CAST(@EndOfYear AS NVARCHAR(8)) + '-12-31' AS DATETIME)

SELECT
Assignment.AssignmentId,
ISNULL(Assignment.AgencyRef, Assignment.AssignmentId) AS AgencyRef, 
0.00 AS TotalActualBill,
YEAR(CASE WHEN @ByPeriodEndDate = 0 THEN TransactionItem.StatusChangeDate ELSE TransactionItem.EndDate END) [Year],
CASE MONTH(CASE WHEN @ByPeriodEndDate = 0 THEN TransactionItem.StatusChangeDate ELSE TransactionItem.EndDate END)
 WHEN 1 THEN 'January'
 WHEN 2 THEN 'February'
 WHEN 3 THEN 'March'
 WHEN 4 THEN 'April'
 WHEN 5 THEN 'May'
 WHEN 6 THEN 'June'
 WHEN 7 THEN 'July'
 WHEN 8 THEN 'August'
 WHEN 9 THEN 'September'
 WHEN 10 THEN 'October'
 WHEN 11 THEN 'November'
 WHEN 12 THEN 'December'
END as [Month],
TransactionItem.BillTotalNetSalesTax AS TotalBill,
TransactionItem.PayTotalNetOfSalesTax AS TotalPay,
TransactionItem.BillUnits AS TotalUnits
FROM Transactionitem
INNER JOIN Assignment ON Assignment.AssignmentId = TransactionItem.AssignmentId
WHERE
 TransactionItem.StatusId = 2 
 AND (@ExcludeExpenseUnits = 1 OR
 CHARINDEX('expense', LOWER(TransactionItem.[BillRateCodeDescription])) > -1)
 AND ((@ByPeriodEndDate = 0 AND (TransactionItem.StatusChangeDate BETWEEN @StartDate AND DATEADD(d, 1, @EndDate)))
 OR (@ByPeriodEndDate = 1 AND (TransactionItem.EndDate BETWEEN @StartDate AND @EndDate)))
 AND (@ConsultantId IS NULL OR Assignment.ConsultantId = @ConsultantId OR Assignment.AccountDirectorId = @ConsultantId OR Assignment.ConsultantSupportingId = @ConsultantId)
ORDER BY AgencyRef
SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
SET ANSI_WARNINGS OFF

DECLARE @StartOfYear INT = 2014;
DECLARE @EndOfYear INT = 2014;
DECLARE @DayHourConversion DECIMAL(2, 1) = 7.5;
DECLARE @ByPeriodEndDate INT = 1;
DECLARE @ExcludeExpenseUnits INT = 0;
DECLARE @ConsultantId INT = 0;

DECLARE @Intval INT
DECLARE @Unitval NVARCHAR(20)
DECLARE @StartDate DATETIME
DECLARE @EndDate DATETIME
IF @ConsultantId = 0 SET @ConsultantId = NULL

SET @Intval = 1
SET @Unitval = 'MONTH'
SET @StartDate = CAST(CAST(@StartOfYear AS NVARCHAR(8)) + '-01-01' AS DATETIME)
SET @EndDate = CAST(CAST(@EndOfYear AS NVARCHAR(8)) + '-12-31' AS DATETIME)

DECLARE @Time_Intervals TABLE (IntervalStart DATETIME, IntervalEnd DATETIME)
DECLARE @ThisDate DATETIME  
DECLARE @NextDate DATETIME
SET @ThisDate = @Startdate    
WHILE (@ThisDate <= @EndDate)  
BEGIN     
 SET @NextDate = (SELECT          
  CASE @Unitval              
   WHEN 'DAY'         THEN DATEADD(DAY, @Intval, @ThisDate)            
   WHEN 'WEEK'        THEN DATEADD(WEEK, @Intval, @ThisDate)            
   WHEN 'MONTH'       THEN DATEADD(MONTH, @Intval, @ThisDate)            
   WHEN 'QUARTER'     THEN DATEADD(QUARTER, @Intval, @ThisDate)            
   WHEN 'YEAR'        THEN DATEADD(YEAR, @Intval, @ThisDate)         
  END)     
 INSERT INTO @Time_Intervals (IntervalStart, IntervalEnd)   
 SELECT @ThisDate, DATEADD(DAY, -1, @NextDate)      
 SET @ThisDate = @NextDate    
END


SELECT
Assignment.AssignmentId,
ISNULL(Assignment.AgencyRef, Assignment.AssignmentId) AS [AgencyRef],
Assignment.StartDate,
Assignment.EndDate,
CASE WHEN Assignment.BillBillingUnit = 2 THEN 1 ELSE @DayHourConversion END * AssignmentRate.BillRate AS DailyForecastBill, --needs multiplying by working day in assignment to make TotalForecastBill
CASE WHEN Assignment.PayBillingUnit = 2 THEN 1 ELSE @DayHourConversion END * AssignmentRate.PayRate AS DailyForecastPay, --similarly, needs multiplying by working days to make TotalForecastPay
CASE WHEN Assignment.BillBillingUnit = 2 THEN 1 ELSE @DayHourConversion END AS DailyWorkUnitForecast, --similarly, needs multiplying by working days to make WorkUnitForecast
DateIntervals.IntervalStart AS IntervalStartDate,
DateIntervals.IntervalEnd AS IntervalEndDate,
CASE MONTH(DateIntervals.IntervalStart)
 WHEN 1 THEN 'January'
 WHEN 2 THEN 'February'
 WHEN 3 THEN 'March'
 WHEN 4 THEN 'April'
 WHEN 5 THEN 'May'
 WHEN 6 THEN 'June'
 WHEN 7 THEN 'July'
 WHEN 8 THEN 'August'
 WHEN 9 THEN 'September'
 WHEN 10 THEN 'October'
 WHEN 11 THEN 'November'
 WHEN 12 THEN 'December'
END AS [Month]
FROM Assignment
CROSS APPLY (SELECT TOP(1) * FROM AssignmentRate WHERE Assignment.AssignmentId = AssignmentRate.AssignmentId) AS AssignmentRate
CROSS JOIN @Time_Intervals AS DateIntervals
WHERE (@ExcludeExpenseUnits = 1 OR
 CHARINDEX('expense', LOWER(AssignmentRate.[BillRateCodeDescription])) > -1)
AND ((Assignment.EndDate BETWEEN @StartDate AND @EndDate)
  OR (Assignment.StartDate BETWEEN @StartDate AND @EndDate)
  OR (Assignment.StartDate < @StartDate AND Assignment.EndDate > @EndDate))
ORDER BY Assignment.AssignmentId, DateIntervals.IntervalStart
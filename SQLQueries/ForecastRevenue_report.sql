SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
SET ANSI_WARNINGS OFF

DECLARE @StartOfYear INT = 2014;
DECLARE @EndOfYear INT = 2014;
DECLARE @DayHourConversion DECIMAL(2, 1) = 7.5;
DECLARE @ByPeriodEndData INT = 1;
DECLARE @ExcludeExpenseUnits INT = 0;
DECLARE @ConsultantId INT = 0;

DECLARE @StartDate DATETIME
DECLARE @EndDate DATETIME
IF @ConsultantId = 0 SET @ConsultantId = NULL
SET @StartDate = CAST(CAST(@StartOfYear AS NVARCHAR(8)) + '-01-01' AS DATETIME)
SET @EndDate = CAST(CAST(@EndOfYear AS NVARCHAR(8)) + '-12-31' AS DATETIME)

SELECT
 Assignment.AssignmentId,
 ISNULL(Assignment.[AgencyRef], Assignment.[AssignmentId]) AS AgencyRef,
 CASE
  WHEN Branch.BranchId IS NULL AND BranchSplit.BranchId IS NULL THEN 'Unassigned'
  WHEN Branch.BranchId = 0 AND BranchSplit.BranchId = 0 THEN 'Unassigned'
  WHEN BranchSplit.BranchId > 0 THEN BranchSplit.Description
  ELSE Branch.Description
 END AS Branch,
 COALESCE(Sector.Description, 'Unassigned') AS Sector,
 CASE 
  WHEN AssignmentConsultantSplit.Split IS NULL THEN COALESCE(AssignmentConsultant.LastFirstName, 'Unassigned')
 ELSE
  COALESCE(AssignmentConsultantSplit.LastFirstName, 'Unassigned') 
 END AS Consultant,
 CAST(CAST(ISNULL(AssignmentConsultantSplit.Split, 1)*100 AS DECIMAL(18,2)) AS NVARCHAR(MAX)) + '%' AS [Consultant Commission %],
 Client.[Name] AS Client,
 ISNULL(Supplier.Name, 'PAYE') AS Supplier,
 Candidate.LastFirstName AS Candidate,
 Assignment.[StartDate],
 Assignment.[EndDate],
 Assignment.[BillCurrencyCode] AS BillCurrency,
 Assignment.[PayCurrencyCode] AS PayCurrency,
 @StartOfYear AS YEAR
FROM
 [dbo].[Assignment]
 INNER JOIN Client ON Assignment.ClientId = Client.ClientId
 INNER JOIN Candidate ON Assignment.CandidateId = Candidate.CandidateId
 LEFT JOIN AssignmentConsultantSplit ON Assignment.AssignmentId = AssignmentConsultantSplit.AssignmentId
 LEFT JOIN AssignmentConsultant ON Assignment.ConsultantId = AssignmentConsultant.ConsultantId
 LEFT JOIN Branch AS BranchSplit ON AssignmentConsultantSplit.BranchId = BranchSplit.BranchId
    LEFT JOIN Branch ON Assignment.BranchId = Branch.BranchId
 LEFT JOIN Sector ON Assignment.SectorId = Sector.SectorId
 LEFT JOIN Supplier ON Assignment.SupplierId = Supplier.SupplierId
 LEFT JOIN PurchaseOrder ON Assignment.PurchaseOrderId = PurchaseOrder.PurchaseOrderId
WHERE 
 (@ConsultantId IS NULL OR Assignment.ConsultantId = @ConsultantId OR Assignment.AccountDirectorId = @ConsultantId OR Assignment.ConsultantSupportingId = @ConsultantId)
 AND ((Assignment.EndDate BETWEEN @StartDate AND @EndDate)
  OR
 (Assignment.StartDate BETWEEN @StartDate AND @EndDate OR Assignment.StartDate < @StartDate AND Assignment.EndDate > @EndDate))
ORDER BY
 Assignment.AssignmentId

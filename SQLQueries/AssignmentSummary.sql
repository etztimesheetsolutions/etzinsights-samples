SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
SET ANSI_WARNINGS OFF

DECLARE @FromEndDate DATETIME = '2014-01-01';
DECLARE @ConsultantId INT = 0;
DECLARE @IncludeAssignmentTypeCSV NVARCHAR(MAX) = NULL;

IF @ConsultantId = 0 SET @ConsultantId = NULL;

SELECT
 Assignment.AgencyRef AS [Agency Reference],
 COALESCE(BranchSplit.[Description], Branch.[Description], 'Unassigned') AS [Branch],
 COALESCE(Sector.[Description], 'Unassigned') AS [Sector],
 Assignment.Ref1,
    Assignment.Ref2,
 COALESCE(Consultant.LastFirstName, 'Unassigned') AS [Consultant],
 Client.Name AS [Client],
 ISNULL(Supplier.Name, 'PAYE') AS [Supplier],
 Candidate.LastFirstName AS [Candidate],
 Assignment.JobTitle AS [Job Title],
 ISNULL(PurchaseOrder.PurchaseOrderNo, '') AS [PO No],
 ISNULL(PurchaseOrder.Amount, 0) AS [PO Amt],
 Assignment.AccountingInterface AS [Interface],
 ISNULL(ConsultantSplit.Split, 1) AS [Split],
 Assignment.StartDate AS [Start Date],
 Assignment.EndDate AS [End Date],
 ISNULL(AssignmentRate.BillRateCodeDescription, 'Unassigned') AS [Bill Description],
 ISNULL(AssignmentRate.BillRate, 0) AS [Bill Rate],
 Assignment.BillCurrencyCode AS [Bill Currency],
 ISNULL(AssignmentRate.PayRateCodeDescription, 'Unassigned') AS [Pay Description],
 ISNULL(AssignmentRate.PayRate, 0) AS [Pay Rate],
    Assignment.PayCurrencyCode AS [Pay Currency],
 (ISNULL(AssignmentRate.BillRate, 0) - ISNULL(AssignmentRate.PayRate, 0)) * ISNULL(ConsultantSplit.Split, 1) AS [GM],
 Datediff (DAY, @FromEndDate,  Assignment.EndDate) AS [Days Left],
 CASE WHEN Assignment.[AllowedAuthorisationTypeMask] & 128 = 128 THEN 'YES' ELSE 'NO' END AS [None Specified],
 CASE WHEN Assignment.[AllowedAuthorisationTypeMask] & 1 = 1 THEN 'YES' ELSE 'NO' END AS [Fax Back],
    CASE WHEN Assignment.[AllowedAuthorisationTypeMask] & 8 = 8 THEN 'YES' ELSE 'NO' END AS [Email & FaxBack],
 CASE WHEN Assignment.[AllowedAuthorisationTypeMask] & 16 = 16 OR (Assignment.[AllowedAuthorisationTypeMask] & 32 = 32) THEN 'YES' ELSE 'NO' END AS [Etz Sign],
 CASE WHEN (Assignment.[AllowedAuthorisationTypeMask] & 2 = 2) OR (Assignment.[AllowedAuthorisationTypeMask] & 4 = 4) THEN 'YES' ELSE 'NO' END AS [Online],
    CASE WHEN Assignment.[AllowedAuthorisationTypeMask] & 64 = 64 THEN 'YES' ELSE 'NO' END AS [Upload Only],
 Assignment.TimesheetTimeSpanName AS [Time Template]
FROM dbo.Assignment
 LEFT OUTER JOIN Branch ON Assignment.BranchId = Branch.BranchId
 LEFT OUTER JOIN Sector ON Assignment.SectorId = Sector.SectorId
 LEFT OUTER JOIN Client ON Assignment.ClientId = Client.ClientId
 LEFT OUTER JOIN Supplier ON Assignment.SupplierId = Supplier.SupplierId
 LEFT OUTER JOIN Candidate ON Assignment.CandidateId = Candidate.CandidateId
 LEFT OUTER JOIN PurchaseOrder ON Assignment.PurchaseOrderId = PurchaseOrder.PurchaseOrderId
 LEFT OUTER JOIN AssignmentRate ON Assignment.AssignmentId = AssignmentRate.AssignmentId
 LEFT OUTER JOIN AssignmentConsultant AS Consultant ON Assignment.ConsultantId = Consultant.ConsultantId
 LEFT OUTER JOIN AssignmentConsultantSplit AS ConsultantSplit ON Assignment.AssignmentId = ConsultantSplit.AssignmentId
 LEFT OUTER JOIN Branch AS BranchSplit ON ConsultantSplit.BranchId = BranchSplit.BranchId
WHERE
 DATEDIFF(DAY, @FromEndDate, Assignment.EndDate) >= 0
 AND (@ConsultantId IS NULL 
  OR Assignment.ConsultantId = @ConsultantId
  OR Assignment.AccountDirectorId = @ConsultantId
  OR Assignment.ConsultantSupportingId = @ConsultantId)
 AND (@IncludeAssignmentTypeCSV IS NULL
  OR CHARINDEX(CAST(Assignment.AssignmentType AS NVARCHAR(MAX)), @IncludeAssignmentTypeCSV) > 0)
ORDER BY
 Client.Name, Candidate.LastName, Consultant.ConsultantId
